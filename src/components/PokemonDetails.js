import React, { Component } from 'react';
import PokemonListItems from './PokemonListItems';
import { getPokemonAndEvolutionsQuery } from '../queries/queries';
import {graphql} from 'react-apollo';

class PokemonDetails extends Component {

  render() { 
    const { data } = this.props;
    if (!data.pokemon) return null;
    return (
        <div>
        <PokemonListItems pokemon={data.pokemon} />
        {data.pokemon.evolutions.map(evolution =>
          <PokemonListItems key={evolution.id} pokemon={evolution} />
        )}            
      </div>
    );
  }
};

export default graphql(getPokemonAndEvolutionsQuery, {
  options(ownProps) {
    return {
      variables: {
          id: ownProps.match.params.id
      },
    };
  }
})(PokemonDetails);